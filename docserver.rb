#!/usr/bin/env ruby
#
# Usage:
# $> ruby docserver.rb
# open browser on http://localhost:4567/
#
require "rubygems"
require "redcloth"
require "sinatra"


# public folder to serve images, css and js if needed.
set :public_folder, 'public'

# Serve index page.
get "/"  do
	serve 'index', "Home"
end

# Serve for any :id page
get "/:id" do
	# read request page
	pagefile = params[:id] == "" ? "index" : params[:id]
	serve pagefile, params[:id]
end

# Serve page pagefile
def serve pagefile, pageName

	headreg = /^\s*h([1-6])\.\s+(.*)/
	pagefile = "pages/" + pagefile + ".textile"
	if( File.exists? pagefile )
		textilePage = IO.read(pagefile)
		textileToc = toc(textilePage, headreg)
		textilePage = createNameTag(textilePage, headreg)		
		document = RedCloth.new(textilePage).to_html
		toc = RedCloth.new(textileToc).to_html
		erb :main, 
			:locals=>{ 
				:page => document, 
				:pageName => pageName,
				:toc => toc}, :format => :html5
	else
		halt 404, "Page not found"
	end
end

def createNameTag(text, headreg)
	text.gsub!(headreg) do |match|
		number = $1
		name = $2
		header = name.gsub(/\s/,"+")
		"\nh" + number + '. <a name="' + header + '">' + name + '</a>'
	end
	return text
end

def toc(text,headreg)
	toc = "" 
	text.gsub(headreg) do |match|
		number = $1
		name = $2
		header = name.gsub(/\s/,"+")
		toc << '#' * number.to_i + ' "' + name + '":#' + header + "\n"
	end
	return toc
end
