# A Ruby Textile doc server
-----

## README


For an internal project, I need to publish dynamicaly some information about projects documentation. To provide such a service on a linux platform, I have choosen a ruby server solution.

### Application structure

Based on Sinatra and RedCloth, I create a VERY small application server, contained into only one file (thanks sinatra).

```
doc/
 |_ pages/
 |  |_ index.textile
 |_ public/
 |  |_ css/
 |  |_ illustrations/
 |_ views/
 |  |_ main.erb
 |_ docserver.rb
 |_ README.md

```


* _docserver.rb_ is the server. 
* _index.textile_ is the welcome page for the doc. 
* _public_ folder contains css, images, and any javascript you will need. 
* the _views/main.erb_ is the template used to generate the HTML pages served by the `docserver.rb`.
* _README.md_ : this file !

### Git repo project

You will found the git repo of this project on [http://bitbucket.org/McGivrer/textile-doc-server](http://bitbucket.org/McGivrer/textile-doc-server)

### Dependencies

Before starting the server, please, install the dependencies:

``` bash
$> sudo gem install sinatra RedCloth
```

### Start server

To start the server, just execute the command line:

``` bash
$> ruby docserver.rb
```

or

``` bash
$> chmod +x docserver.rb
$> ./docserver.rb
```
And open your browser to [http://localhost:4567/](http://localhost:4567/)

Have Fun !

McG.